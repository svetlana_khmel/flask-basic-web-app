** The goal of this project:** 
* Create JWT authorization
* Create REST









https://www.freecodecamp.org/news/how-to-build-a-web-application-using-flask-and-deploy-it-to-the-cloud-3551c985e492/

  503  cd venv/
  504   python main.py
  505  source bin/activate
  506  export FLASK_APP=hello.py
  507  flask run
  508  pip install flask
  509  flask run
  510  python main.py
  511  pip install -t lib -r requirements.txt




Before you Start:
You will need a Google Account. Once you create an account, go to the Google Cloud Platform Console and create a new project. Also, you need to install the Google Cloud SDK.

If you were to check Google’s Tutorial in the part where they talk about content of the app.yaml, it does not include the section where I wrote about libraries.

When I first attempted to deploy my simple web app, my deployment never worked. After many attempts, I learned that we needed to include the SSL library.

The SSL Library allows us to create secure connections between the client and server. Every time the user goes to our website they will need to connect to a server run by Google App Engine. We need to create a secure connection for this.

Now inside our virtual environment (make sure your virtualenv is activated), we are going to install the new dependencies we have in requirements.txt. Run this command:

pip install -t lib -r requirements.txt
-t lib: This flag copies the libraries into a lib folder, which uploads to App Engine during deployment.

-r requirements.txt: Tells pip to install everything from requirements.txt.

For more information on how to get started, please visit:
  https://cloud.google.com/sdk/docs/quickstarts
  https://console.cloud.google.com/appengine/start/deploy?language=python&environment=standard&project=hip-phoenix-272417&folder&organizationId

- Initialize your SDK
gcloud init

- Deploy to App Engine
gcloud app deploy
  
  For further information about the command-line tools for Google App Engine, Compute Engine, Cloud Storage, BigQuery, Cloud SQL and Cloud DNS (which are all bundled with Cloud SDK), see Accessing Services with the gcloud CLI.
  https://cloud.google.com/sdk/cloudplatform
  
  gcloud init
  gcloud app deploy
  
  
  
  
  See https://cloud.google.com/compute/docs/gcloud-compute section on how to set
default compute region and zone manually. If you would like [gcloud init] to be
able to do this for you the next time you run it, make sure the
Compute Engine API is enabled for your project on the
https://console.developers.google.com/apis page.

Created a default .boto configuration file at [/Users/svitlana/.boto]. See this file and
[https://cloud.google.com/storage/docs/gsutil/commands/config] for more
information about configuring Google Cloud Storage.
Your Google Cloud SDK is configured and ready to use!

* Commands that require authentication will use sinergy.ua@gmail.com by default
* Commands will reference project `test-drive-205521` by default
Run `gcloud help config` to learn how to change individual settings

This gcloud configuration is called [default]. You can create additional configurations if you work with multiple accounts and/or projects.
Run `gcloud topic configurations` to learn more.

Some things to try next:

* Run `gcloud --help` to see the Cloud Platform services you can interact with. And run `gcloud help COMMAND` to get help on any gcloud command.
* Run `gcloud topic --help`

Available here:
https://test-drive-205521.appspot.com




CRUD: Building a CRUD application with Flask and SQLAlchemy
https://scotch.io/tutorials/build-a-crud-web-app-with-python-and-flask-part-one
https://github.com/sixhobbits/flask-crud-app

Flask-SQLAlchemy: This will allow us to use SQLAlchemy, a useful tool for SQL use with Python. SQLAlchemy is an Object Relational Mapper (ORM), which means that it connects the objects of an application to tables in a relational database management system. These objects can be stored in the database and accessed without the need to write raw SQL. This is convenient because it simplifies queries that may have been complex if written in raw SQL. Additionally, it reduces the risk of SQL injection attacks since we are not dealing with the input of raw SQL.

MySQL-Python: This is a Python interface to MySQL. It will help us connect the MySQL database to the app.

$ pip install flask-sqlalchemy mysql-python (pip install mysqlclient ---> instead of mysql-python in Python v3)

https://www.codementor.io/@garethdwyer/building-a-crud-application-with-flask-and-sqlalchemy-dm3wv7yu2

* How to work with sqlite:*
https://www.youtube.com/watch?v=iyXYwNQC6ag



Initializing our database:
 Run the following commands in a Python shell in your project directory in order to create our database and create the book table where we'll store our books. 
 You can start a Python shell by running python3 in your console (making sure you are in the project directory).
 You can start a Python shell by running python3
>>> from bookmanager import db
>>> db.create_all()
>>> exit()


* Include one file to another:
test-routes.py
`
from __main__ import app

@app.route('/test', methods=['GET'])
def test():
    return 'it works!'
and in your main files, where you declared flask app, import test-routes, like:

app.py

from flask import Flask, request, abort

app = Flask(__name__)

# import declared routes
import test-routes
`    
    
    
    
** Flask-SQLAlchemy-Session **
https://flask-sqlalchemy-session.readthedocs.io/en/v1.1/    
    
    
    
** Things to do **
* Add more components to the books, including Author, Publisher, and Date of Publication.
* Split off Author and Publisher into their own tables, so that we can represent complicated relationships among books and authors and publishers (each author can have many publishers and many books; each book can have many authors and publishers; each publisher can have many authors and books).
* Display error messages to the user instead of just logging and ignoring them in the back-end code.
* Make the application more aesthetically pleasing by adding some CSS, either custom CSS or through a framework like Bootstrap.    
* Use visualisation library
* table with pagination and sorting
* Authentification with sessions
* Try another database
* Try docker



Bootstrap:
pip install flask-bootstrap
https://pythonhosted.org/Flask-Bootstrap/basic-usage.html
from flask_bootstrap import Bootstrap
Bootstrap(app)
{% extends "bootstrap/base.html" %}

<!--{% block content %}-->
<!-- {{ super() }}-->
<!--  <h1>Hello, Bootstrap</h1>-->
<!--{% endblock %}-->



----------
pip install flask-restful


TROUBLESHOOTING:
Flask ImportError: No Module Named Flask

Try deleting the virtualenv you created. Then create a new virtualenv with:

virtualenv flask
Then:

cd flask
Now let's activate the virtualenv:

source bin/activate
Now you should see (flask) on the left of the command line.

Let's install flask:

pip install flask
Then create a file named hello.py (NOTE: see UPDATE Flask 1.0.2 below):

from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run()
and run it with:

python hello.py
UPDATE Flask 1.0.2

With the new flask release there is no need to run the app from your script. hello.py should look like this now:

from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"
and run it with:

FLASK_APP=hello.py flask run
Make sure to be inside the folder where hello.py is when running the latest command.


** Avoiding the "405 Method Not Allowed" Error in Flask **
https://www.youtube.com/watch?v=q5rIxpE3fjA

** SQLite Tutorial: Using SQLite Database on Mac! ** 
https://www.youtube.com/watch?v=iyXYwNQC6ag

** sqlitebrowser **

*** Installing Python 3 on Mac OS X ***
https://python-docs.readthedocs.io/en/latest/starting/install3/osx.html
The latest version of Mac OS X, High Sierra, comes with Python 2.7 out of the box.

You do not need to install or configure anything else to use Python 2. These instructions document the installation of Python 3.

The version of Python that ships with OS X is great for learning, but it’s not good for development. The version shipped with OS X may be out of date from the official current Python release, which is considered the stable production version.

Doing it Right
Let’s install a real version of Python.

Before installing Python, you’ll need to install GCC. GCC can be obtained by downloading XCode, the smaller Command Line Tools (must have an Apple account) or the even smaller OSX-GCC-Installer package.

Note
If you already have XCode installed, do not install OSX-GCC-Installer. In combination, the software can cause issues that are difficult to diagnose.

Note
If you perform a fresh install of XCode, you will also need to add the commandline tools by running xcode-select --install on the terminal.

While OS X comes with a large number of UNIX utilities, those familiar with Linux systems will notice one key component missing: a package manager. Homebrew fills this void.

To install Homebrew, open Terminal or your favorite OSX terminal emulator and run

$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
The script will explain what changes it will make and prompt you before the installation begins. Once you’ve installed Homebrew, insert the Homebrew directory at the top of your PATH environment variable. You can do this by adding the following line at the bottom of your ~/.profile file

export PATH=/usr/local/bin:/usr/local/sbin:$PATH
Now, we can install Python 3:

$ brew install python
This will take a minute or two.

Pip
Homebrew installs pip pointing to the Homebrew’d Python 3 for you.

Working with Python 3
At this point, you have the system Python 2.7 available, potentially the Homebrew version of Python 2 installed, and the Homebrew version of Python 3 as well.

$ python
will launch the homebrew-installed Python 3 interpreter.

$ python2
will launch the homebrew-installed Python 2 interpreter (if any).

$ python3
will launch the homebrew-installed Python 3 interpreter.

If the Homebrew version of Python 2 is installed then pip2 will point to Python 2. If the Homebrew version of Python 3 is installed then pip will point to Python 3.

The rest of the guide will assume that python references Python 3.

# Do I have a Python 3 installed?
$ python --version
Python 3.6.4 # Success!
# If you still see 2.7 ensure in PATH /usr/local/bin/ takes precedence over /usr/bin/
Pipenv & Virtual Environments
The next step is to install Pipenv, so you can install dependencies and manage virtual environments.

A Virtual Environment is a tool to keep the dependencies required by different projects in separate places, by creating virtual Python environments for them. It solves the “Project X depends on version 1.x but, Project Y needs 4.x” dilemma, and keeps your global site-packages directory clean and manageable.

For example, you can work on a project which requires Django 1.10 while also maintaining a project which requires Django 1.8.

So, onward! To the Pipenv & Virtual Environments docs!




-----------------------------------------------


!! https://www.linkedin.com/learning/building-restful-apis-with-flask
Mailtrap.io
flask_login - log in, log out, session mangement
flask_user - user registration, log in, log out
Jwt.io
flask_mail
Pip freeze - generates a list with requirements
pythonAnywhere.com
Digitalocean.com
Gunicorn.org    
Nginx.com
>> Learning Python with PyCharm with Bruce Van Horn
Linux: Web Services with Scott Simpson

If db is your SQLAlchemy object, what methods do you need to call to create, delete, and seed the database?

* db.create_all, db.drop_all, db.session.add 

------------------------------------------------
sqlite3 comment_section.db

.show	Displays current settings for various parameters
.databases	Provides database names and files
.quit	Quit sqlite3 program
.tables	Show current tables
.schema	Display schema of table
.header	Display or hide the output table header
.mode	Select mode for the output table
.dump	Dump database in SQL text format

sqlite3 example.db
.schema table
man sqlite3

-----------------------------------------------

python3 -m venv niceproject
cd niceproject
source niceproject/bin/activate

deactivate



 
### Install and config Redis on Mac OS X via Homebrew
```
By using Homebrew, you greatly reduce the cost of setting up and configuring the development environment on Mac OS X.
Let’s install Redis for the good.
$ brew install redis
After installation, you will see some notification about some caveats on configuring. Just leave it and continue to following some tasks on this article.
Launch Redis on computer starts.
$ ln -sfv /usr/local/opt/redis/*.plist ~/Library/LaunchAgents
Start Redis server via “launchctl”.
$ launchctl load ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
Start Redis server using configuration file.
$ redis-server /usr/local/etc/redis.conf
Stop Redis on autostart on computer start.
$ launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
Location of Redis configuration file.
/usr/local/etc/redis.conf
Uninstall Redis and its files.
$ brew uninstall redis
$ rm ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
Get Redis package information.
$ brew info redis
Test if Redis server is running.
$ redis-cli ping
If it replies “PONG”, then it’s good to go!

```
### Blacklist and Token Revoking
- https://flask-jwt-extended.readthedocs.io/en/stable/blacklist_and_token_revoking/

### Redis backlist example
- https://github.com/vimalloc/flask-jwt-extended/blob/master/examples/database_blacklist/app.py


### JWT in Cookies
- https://flask-jwt-extended.readthedocs.io/en/stable/tokens_in_cookies/
- https://github.com/vimalloc/flask-jwt-extended/blob/master/examples/redis_blacklist.py
###


#### CSV
- https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv













  