from flask import Flask, jsonify, request, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String, Float
import os
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, jwt_refresh_token_required, create_refresh_token, get_jwt_identity, set_access_cookies, set_refresh_cookies, unset_jwt_cookies

from __main__ import app

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JWT_TOKEN_LOCATION'] = ['cookies']
app.config['JWT_ACCESS_COOKIE_PATH'] = '/api/'
app.config['JWT_REFRESH_COOKIE_PATH'] = '/token/refresh'
app.config['JWT_COOKIE_CSRF_PROTECT'] = False

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'planets.db')
app.config['JWT_SECRET_KEY'] = 'super-secret'  # change this IRL
# app.config['MAIL_SERVER'] = 'smtp.mailtrap.io'
# app.config['MAIL_USERNAME'] = os.environ['MAIL_USERNAME']
# app.config['MAIL_PASSWORD'] = os.environ['MAIL_PASSWORD']

db = SQLAlchemy(app)
ma = Marshmallow(app)
jwt = JWTManager(app)
# mail = Mail(app)


@app.route('/not_found')
def not_found():
    return jsonify(message='That resource was not found'), 404

# @app.route("/about")
# def about():
#     return render_template('about.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/api/register_user', methods=['POST'])
def register_user():
    email = request.form['email']
    test = User.query.filter_by(email=email).first()
    if test:
        return jsonify(message='That email already exists.'), 409
    else:
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        password = request.form['password']
        user = User(first_name=first_name, last_name=last_name, email=email, password=password)
        db.session.add(user)
        db.session.commit()

        #return jsonify(message="User created successfully."), 201
        return render_template('login.html', message="User created successfully.")


# Use the set_access_cookie() and set_refresh_cookie() on a response
# object to set the JWTs in the response cookies. You can configure
# the cookie names and other settings via various app.config options
# @app.route('/api/login_user', methods=['POST'])
# def token_auth():
#     if request.is_json:
#         user = request.json['username']
#         email = request.json['email']
#         password = request.json['password']
#     else:
#         email = request.form['email']
#         password = request.form['password']
#
#     # if email != 'test' or password != 'test':
#     #     return jsonify({'login': False}), 401
#     user = User.query.filter_by(email=email, password=password).first()
#     if user:
#         # Create the tokens we will be sending back to the user
#         access_token = create_access_token(identity=email)
#         refresh_token = create_refresh_token(identity=email)
#
#         # Set the JWT cookies in the response
#         resp = jsonify({'login': True})
#         set_access_cookies(resp, access_token)
#         set_refresh_cookies(resp, refresh_token)
#         #return resp, 200
#         user.is_authenticated = True
#         user.email = email
#
#         return render_template('private.html', message="Login succeeded!", user=user)

# Same thing as login here, except we are only setting a new cookie
# for the access token.
@app.route('/token/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    # Create the new access token
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)

    # Set the JWT access cookie in the response
    resp = jsonify({'refresh': True})
    set_access_cookies(resp, access_token)
    return resp, 200

# Because the JWTs are stored in an httponly cookie now, we cannot
# log the user out by simply deleting the cookie in the frontend.
# We need the backend to send us a response to delete the cookies
# in order to logout. unset_jwt_cookies is a helper function to
# do just that.
@app.route('/logout', methods=['GET'])
def logout():
    resp = jsonify({'logout': True})
    unset_jwt_cookies(resp)
    print('bb')

    return redirect(url_for('home'))
    #return resp, 200


# We do not need to make any changes to our protected endpoints. They
# will all still function the exact same as they do when sending the
# JWT in via a header instead of a cookie
@app.route('/api/example', methods=['GET'])
@jwt_required
def protected():
    username = get_jwt_identity()
    return jsonify({'hello': 'from {}'.format(username)}), 200

@app.route('/api/private', methods=['GET'])
@jwt_required
def private():
    email = get_jwt_identity()
    user = User.query.filter_by(email=email).first()
    user.email = user
    user.is_authenticated  = True

    return render_template('private.html', user=user)

# @jwt_required
# def protected():
#     username = get_jwt_identity()
#     return jsonify({'hello': 'from {}'.format(username)}), 200


@app.route('/api/login_user', methods=['POST'])
def login_user():
    if request.is_json:
        #username = request.json['username']
        email = request.json['email']
        password = request.json['password']
    else:
        email = request.form['email']
        password = request.form['password']

    user = User.query.filter_by(email=email, password=password).first()
    if user:
        # Create the tokens we will be sending back to the user
        access_token = create_access_token(identity=email)
        refresh_token = create_refresh_token(identity=email)
        #return jsonify(message="Login succeeded!", access_token=access_token)
        user.is_authenticated = True


        # Set the JWT cookies in the response
        resp = jsonify({'login': True})
        set_access_cookies(resp, access_token)
        set_refresh_cookies(resp, refresh_token)
        return render_template('private.html', message="Login succeeded!", user=user)
        #return resp, 200
    else:
        return jsonify(message="Bad email or password"), 401

class User(db.Model):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String, unique=True)
    password = Column(String)

class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'first_name', 'last_name', 'email', 'password')

user_schema = UserSchema()
users_schema = UserSchema(many=True)
db.create_all()

if __name__ == '__main__':
    app.run()


