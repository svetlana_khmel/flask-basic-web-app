from flask import Flask, render_template
# from flask_restful import Resource, Api
app = Flask(__name__)

import bookmanager
import restful
import auth


# with open('bookmanager.py') as infile:
#    exec(infile.read())

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/about")
def about():
    return render_template('about.html')

@app.route("/admin")
def admin():
    return render_template('admin.html')

# class HelloWorld(Resource):
#     def get(self):
#         return {'hello': 'world'}
#
# api.add_resource(HelloWorld, '/hello')

if __name__ == "__main__":
    app.run(debug=True)



