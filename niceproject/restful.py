from __main__ import app
from flask_restful import Resource, Api
api = Api(app)

TODOS = {
    'todo1': {'task': 'build an API'},
    'todo2': {'task': '?????'},
    'todo3': {'task': 'profit!'},
}

def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        abort(404, message="Todo {} doesn't exist".format(todo_id))

class Todo(Resource):
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        return TODOS[todo_id]

class TodoList(Resource):
    def get(self):
        return TODOS

class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

#api.add_resource(HelloWorld, '/hello')

api.add_resource(HelloWorld,
    '/hello1',
    '/hello')

api.add_resource(TodoList, '/todos')
api.add_resource(Todo, '/todos/<todo_id>')